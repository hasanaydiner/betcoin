﻿#pragma checksum "..\..\MainWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "1DD083CC25F4DF5D79AD8142FEDF1687401975DD3481E22759B8343DEF95C601"
//------------------------------------------------------------------------------
// <auto-generated>
//     Bu kod araç tarafından oluşturuldu.
//     Çalışma Zamanı Sürümü:4.0.30319.42000
//
//     Bu dosyada yapılacak değişiklikler yanlış davranışa neden olabilir ve
//     kod yeniden oluşturulursa kaybolur.
// </auto-generated>
//------------------------------------------------------------------------------

using Crypto;
using MaterialDesignExtensions.Controls;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Crypto {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 26 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ColumnDefinition MenuGenislik;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ColumnDefinition OrtaPanelGenislik;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid board_kaydir;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image ButonAcilirMenu;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image LogoYazi;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button menuButon_piyasa;
        
        #line default
        #line hidden
        
        
        #line 100 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label MenuYazi1;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button menuButon_varliklar;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label MenuYazi2;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button menuButon_hesapDetay;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label MenuYazi3;
        
        #line default
        #line hidden
        
        
        #line 116 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button menuButon_karZarar;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label MenuYazi4;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button menuButon_bankaHesap;
        
        #line default
        #line hidden
        
        
        #line 125 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label MenuYazi5;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button menuButon_sifreDegistir;
        
        #line default
        #line hidden
        
        
        #line 131 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label MenuYazi6;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label MenuYazi7;
        
        #line default
        #line hidden
        
        
        #line 224 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button butonSimgeDurumu;
        
        #line default
        #line hidden
        
        
        #line 229 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buton_kapat;
        
        #line default
        #line hidden
        
        
        #line 266 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock limit;
        
        #line default
        #line hidden
        
        
        #line 267 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock stop;
        
        #line default
        #line hidden
        
        
        #line 268 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel limit_cizgi;
        
        #line default
        #line hidden
        
        
        #line 269 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stop_cizgi;
        
        #line default
        #line hidden
        
        
        #line 282 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock al;
        
        #line default
        #line hidden
        
        
        #line 283 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock sat;
        
        #line default
        #line hidden
        
        
        #line 287 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid tetiklenen;
        
        #line default
        #line hidden
        
        
        #line 298 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tetik_textbox;
        
        #line default
        #line hidden
        
        
        #line 324 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox fiyat_textbox;
        
        #line default
        #line hidden
        
        
        #line 349 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock miktar_coinName;
        
        #line default
        #line hidden
        
        
        #line 353 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox miktar_textbox;
        
        #line default
        #line hidden
        
        
        #line 381 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox toplam_textbox;
        
        #line default
        #line hidden
        
        
        #line 401 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button islemYapButon;
        
        #line default
        #line hidden
        
        
        #line 443 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar DeterminateCircularProgress;
        
        #line default
        #line hidden
        
        
        #line 450 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock sdu;
        
        #line default
        #line hidden
        
        
        #line 451 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock sdu_fiyat;
        
        #line default
        #line hidden
        
        
        #line 455 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock maku;
        
        #line default
        #line hidden
        
        
        #line 456 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock maku_fiyat;
        
        #line default
        #line hidden
        
        
        #line 460 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock otdu;
        
        #line default
        #line hidden
        
        
        #line 461 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock otdu_fiyat;
        
        #line default
        #line hidden
        
        
        #line 492 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label Kullanilabilir_Bakiye;
        
        #line default
        #line hidden
        
        
        #line 500 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label KullaniciAdi;
        
        #line default
        #line hidden
        
        
        #line 501 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label KullaniciSoyadi;
        
        #line default
        #line hidden
        
        
        #line 524 "..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid OrtaPanelIcerik;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Crypto;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 17 "..\..\MainWindow.xaml"
            ((Crypto.MainWindow)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.MenuGenislik = ((System.Windows.Controls.ColumnDefinition)(target));
            return;
            case 3:
            this.OrtaPanelGenislik = ((System.Windows.Controls.ColumnDefinition)(target));
            return;
            case 4:
            this.board_kaydir = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            
            #line 54 "..\..\MainWindow.xaml"
            ((System.Windows.Controls.DockPanel)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.DockPanel_MouseDown);
            
            #line default
            #line hidden
            return;
            case 6:
            this.ButonAcilirMenu = ((System.Windows.Controls.Image)(target));
            
            #line 56 "..\..\MainWindow.xaml"
            this.ButonAcilirMenu.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.ButonAcilirMenu_MouseDown);
            
            #line default
            #line hidden
            return;
            case 7:
            this.LogoYazi = ((System.Windows.Controls.Image)(target));
            
            #line 71 "..\..\MainWindow.xaml"
            this.LogoYazi.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.LogoYazi_MouseDown);
            
            #line default
            #line hidden
            return;
            case 8:
            this.menuButon_piyasa = ((System.Windows.Controls.Button)(target));
            
            #line 97 "..\..\MainWindow.xaml"
            this.menuButon_piyasa.Click += new System.Windows.RoutedEventHandler(this.menuButon_piyasa_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.MenuYazi1 = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.menuButon_varliklar = ((System.Windows.Controls.Button)(target));
            
            #line 104 "..\..\MainWindow.xaml"
            this.menuButon_varliklar.Click += new System.Windows.RoutedEventHandler(this.menuButon_varliklar_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.MenuYazi2 = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.menuButon_hesapDetay = ((System.Windows.Controls.Button)(target));
            
            #line 110 "..\..\MainWindow.xaml"
            this.menuButon_hesapDetay.Click += new System.Windows.RoutedEventHandler(this.menuButon_hesapDetay_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.MenuYazi3 = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.menuButon_karZarar = ((System.Windows.Controls.Button)(target));
            
            #line 116 "..\..\MainWindow.xaml"
            this.menuButon_karZarar.Click += new System.Windows.RoutedEventHandler(this.menuButon_karZarar_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.MenuYazi4 = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.menuButon_bankaHesap = ((System.Windows.Controls.Button)(target));
            
            #line 122 "..\..\MainWindow.xaml"
            this.menuButon_bankaHesap.Click += new System.Windows.RoutedEventHandler(this.menuButon_bankaHesap_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.MenuYazi5 = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.menuButon_sifreDegistir = ((System.Windows.Controls.Button)(target));
            
            #line 128 "..\..\MainWindow.xaml"
            this.menuButon_sifreDegistir.Click += new System.Windows.RoutedEventHandler(this.menuButon_sifreDegistir_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.MenuYazi6 = ((System.Windows.Controls.Label)(target));
            return;
            case 20:
            this.MenuYazi7 = ((System.Windows.Controls.Label)(target));
            return;
            case 21:
            this.butonSimgeDurumu = ((System.Windows.Controls.Button)(target));
            
            #line 224 "..\..\MainWindow.xaml"
            this.butonSimgeDurumu.Click += new System.Windows.RoutedEventHandler(this.butonSimgeDurumu_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.buton_kapat = ((System.Windows.Controls.Button)(target));
            
            #line 229 "..\..\MainWindow.xaml"
            this.buton_kapat.Click += new System.Windows.RoutedEventHandler(this.buton_kapat_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.limit = ((System.Windows.Controls.TextBlock)(target));
            
            #line 266 "..\..\MainWindow.xaml"
            this.limit.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.limit_MouseDown);
            
            #line default
            #line hidden
            return;
            case 24:
            this.stop = ((System.Windows.Controls.TextBlock)(target));
            
            #line 267 "..\..\MainWindow.xaml"
            this.stop.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.stop_MouseDown);
            
            #line default
            #line hidden
            return;
            case 25:
            this.limit_cizgi = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 26:
            this.stop_cizgi = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 27:
            this.al = ((System.Windows.Controls.TextBlock)(target));
            
            #line 282 "..\..\MainWindow.xaml"
            this.al.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.al_MouseDown);
            
            #line default
            #line hidden
            return;
            case 28:
            this.sat = ((System.Windows.Controls.TextBlock)(target));
            
            #line 283 "..\..\MainWindow.xaml"
            this.sat.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.sat_MouseDown);
            
            #line default
            #line hidden
            
            #line 283 "..\..\MainWindow.xaml"
            this.sat.MouseEnter += new System.Windows.Input.MouseEventHandler(this.sat_MouseEnter);
            
            #line default
            #line hidden
            
            #line 283 "..\..\MainWindow.xaml"
            this.sat.MouseLeave += new System.Windows.Input.MouseEventHandler(this.sat_MouseLeave);
            
            #line default
            #line hidden
            return;
            case 29:
            this.tetiklenen = ((System.Windows.Controls.Grid)(target));
            return;
            case 30:
            this.tetik_textbox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 31:
            this.fiyat_textbox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 32:
            this.miktar_coinName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 33:
            this.miktar_textbox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 34:
            this.toplam_textbox = ((System.Windows.Controls.TextBox)(target));
            
            #line 390 "..\..\MainWindow.xaml"
            this.toplam_textbox.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.toplam_textbox_TextChanged);
            
            #line default
            #line hidden
            return;
            case 35:
            this.islemYapButon = ((System.Windows.Controls.Button)(target));
            
            #line 405 "..\..\MainWindow.xaml"
            this.islemYapButon.Click += new System.Windows.RoutedEventHandler(this.islemYapButon_Click);
            
            #line default
            #line hidden
            return;
            case 36:
            this.DeterminateCircularProgress = ((System.Windows.Controls.ProgressBar)(target));
            return;
            case 37:
            this.sdu = ((System.Windows.Controls.TextBlock)(target));
            
            #line 450 "..\..\MainWindow.xaml"
            this.sdu.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.sdu_MouseDown);
            
            #line default
            #line hidden
            return;
            case 38:
            this.sdu_fiyat = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 39:
            this.maku = ((System.Windows.Controls.TextBlock)(target));
            
            #line 455 "..\..\MainWindow.xaml"
            this.maku.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.maku_MouseDown);
            
            #line default
            #line hidden
            return;
            case 40:
            this.maku_fiyat = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 41:
            this.otdu = ((System.Windows.Controls.TextBlock)(target));
            
            #line 460 "..\..\MainWindow.xaml"
            this.otdu.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.otdu_MouseDown);
            
            #line default
            #line hidden
            return;
            case 42:
            this.otdu_fiyat = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 43:
            this.Kullanilabilir_Bakiye = ((System.Windows.Controls.Label)(target));
            return;
            case 44:
            this.KullaniciAdi = ((System.Windows.Controls.Label)(target));
            return;
            case 45:
            this.KullaniciSoyadi = ((System.Windows.Controls.Label)(target));
            return;
            case 46:
            this.OrtaPanelIcerik = ((System.Windows.Controls.Grid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

